#!/usr/bin/env raku

use Timer::Stopwatch;

my $irregular-supply = Supply.interval(1).grep: { Bool.pick }

my $timer = Timer::Stopwatch.new;
react {
    whenever $irregular-supply {
        note "{ now.DateTime.hh-mm-ss }: Received an irregular event";

        # Wait up to 2 seconds for the next event
        $timer.reset: 2;
    }
    whenever $timer {
        note "It's been { .round } seconds since the last event";
        $timer.stop;
    }
    whenever Promise.in: 20 {
        note 'Stopping after 20 seconds';
        $timer.stop;
    }
    whenever $timer.Promise {
        note "Timer was stopped. We're done";
        done;
    }
}

# OUTPUT:
# 20:33:39: Received an irregular event
# 20:33:40: Received an irregular event
# 20:33:41: Received an irregular event
# It's been 2 seconds since the last event
# Timer was stopped. We're done
