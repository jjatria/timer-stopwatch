use Test;
use Timer::Stopwatch;

subtest 'Start interface' => {
    my $timer = Timer::Stopwatch.new;
    sleep 0.1;
    is $timer.Duration.round(0.1), 0.1, 'Can get the Duration of a stopwatch';

    is $timer.reset, False, 'Can reset, and result is False if no timer has been set';
    is $timer.Duration.round(0.1), 0, 'Reset alters the start of the Duration';
}

subtest 'Supply interface' => {
    my $timer = Timer::Stopwatch.in: 0.1;
    my $resets;

    react {
        whenever Promise.in: 0.4 {
            flunk 'Timer did not complete before timeout' unless $timer.Promise;
            done;
        }
        whenever $timer {
            .&isa-ok: Duration, 'Supply receives Durations';

            is $timer.reset(0.1), True,
                'Reset returns True when timer is interrupted';

            $timer.stop if ++$resets >= 3;
        }
        whenever $timer.Promise {
            pass 'Timer completed before timeout';
            done;
        }
    }

    is $resets, 3, 'Completed 3 timers';
    is $timer.reset, False, 'Reset returns False when timer has completed';
}

subtest 'Idempotency' => {
    my $timer = Timer::Stopwatch.new: in => 0.5;
    is $timer.stop, True,  'Returns True because it interrupted a timer';
    is $timer.stop, False, 'Returns False because stopwatch was stopped';
}

done-testing;
