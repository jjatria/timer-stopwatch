use Timer::Breakable;
use OO::Monitors;

=begin pod

=head2 NAME

Timer::Stopwatch - Schedule and reset repeated time measurements

=head2 SYNOPSIS

=begin code

use Timer::Stopwatch;

my $irregular-supply = Supply.interval(1).grep: { Bool.pick }

my $timer = Timer::Stopwatch.new;
react {
    whenever $irregular-supply {
        note "{ now.DateTime.hh-mm-ss }: Received an irregular event";

        # Wait up to 2 seconds for the next event
        $timer.reset: 2;
    }
    whenever $timer {
        note "It's been { .round } seconds since the last event";
        $timer.stop;
    }
    whenever Promise.in: 20 {
        note 'Stopping after 20 seconds';
        $timer.stop;
    }
    whenever $timer.Promise {
        note "Timer was stopped. We're done";
        done;
    }
}

# OUTPUT:
# 20:33:39: Received an irregular event
# 20:33:40: Received an irregular event
# 20:33:42: Received an irregular event
# It's been 2 seconds since the last event
# Timer was stopped. We're done

=end code

=head2 DESCRIPTION

Timer::Stopwatch is a resettable, stoppable wrapper around a Supply
that can be used to mark multiple moments in time.

=end pod

unit monitor Timer::Stopwatch:ver<0.1.1>:auth<zef:jjatria>;

has Promise          $!done     .= new;
has Supplier         $!supplier .= new;
has Instant          $!start     = now;
has Timer::Breakable $!timer;

=head2 METHODS

#|(
    Creates a new Stopwatch with a timer that will trigger in the
    given amount of seconds, unless it is reset or stopped before that.
)
method in ( ::?CLASS:U: Numeric $in --> Timer::Stopwatch ) {
    self.bless: :$in;
}

submethod TWEAK (:$in) {
    $!timer = Timer::Breakable.start: $in, { self.tick } if $in;
}

#|(
    Emits the duration since creation time (or the last C<reset>)
    to the Supply. Returns the emitted Duration.
)
method tick ( ::?CLASS:D: --> Duration ) {
    my $dur = self.Duration;
    $!supplier.emit: $dur;
    return $dur;
}

#|(
    Reset sets the start time for the stopwatch to the current time.
    If called with a numeric argument, it also sets a timer to expire
    after the given duration.

    It returns True if the call interrupted a pending timer, and False if
    no timer had been set, or if it had already expired, or if the stopwatch
    had already been stopped.
)
method reset ( ::?CLASS:D: Numeric $in? --> Bool ) {
    return False if $!done;
    $!start = now;
    my $status = self!stop;
    $!timer = Timer::Breakable.start: $in, { self.tick } if $in;
    return $status;
}

#|(
    Stop marks this stopwatch as done. Once a stopwatch has been stopped,
    most of its actions are finished, and new stopwatch should be created.
    Internally, it stops any pending timers and closes the supply.

    It returns True if the call interrupted a pending timer, and False if
    no timer had been set, or if it had already expired, or if the stopwatch
    had already been stopped.
)
method stop ( ::?CLASS:D: --> Bool ) {
    return False if $!done;
    $!supplier.done;
    $!done.keep: self.Duration;
    return self!stop;
}

#|(
    Returns the amount of time elapsed since the start (or the last reset)
    as a Duration.
)
method Duration ( ::?CLASS:D: --> Duration ) { now - $!start }

#|(
    Returns a Promise that will be kept when the Stopwatch is stopped.
    When kept, it will hold the Duration elapsed since the time the
    Stopwatch was created, or since the last call to C<reset>.
)
method Promise ( ::?CLASS:D: --> Promise ) { $!done }

#| Returns a live Supply that will receive timer events
method Supply ( ::?CLASS:D: --> Supply ) { $!supplier.Supply }

# Private methods

# Private stop method. Called from reset and stop. Returns False if no timer
# is set, or if the timer is still running. Otherwise, it stops the timer and
# returns True.
method !stop ( ::?CLASS:D: --> Bool ) {
    return False unless $!timer;
    $!timer.stop;
    return True;
}

=begin pod

=head2 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head2 COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it
under the Artistic License 2.0.

=end pod
