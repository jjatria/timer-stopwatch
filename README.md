## NAME

Timer::Stopwatch - Schedule and reset repeated time measurements

## SYNOPSIS

```perl6
use Timer::Stopwatch;

my $irregular-supply = Supply.interval(1).grep: { Bool.pick }

my $timer = Timer::Stopwatch.new;
react {
    whenever $irregular-supply {
        note "{ now.DateTime.hh-mm-ss }: Received an irregular event";

        # Wait up to 2 seconds for the next event
        $timer.reset: 2;
    }
    whenever $timer {
        note "It's been { .round } seconds since the last event";
        $timer.stop;
    }
    whenever Promise.in: 20 {
        note 'Stopping after 20 seconds';
        $timer.stop;
    }
    whenever $timer.Promise {
        note "Timer was stopped. We're done";
        done;
    }
}

# OUTPUT:
# 20:33:39: Received an irregular event
# 20:33:40: Received an irregular event
# 20:33:42: Received an irregular event
# It's been 2 seconds since the last event
# Timer was stopped. We're done
```

## DESCRIPTION

Timer::Stopwatch is a resettable, stoppable wrapper around a Supply that can be used to mark multiple moments in time.

## METHODS

### in

```perl6
method in(
    Numeric $in
) returns Timer::Stopwatch
```

Creates a new Stopwatch with a timer that will trigger in the given amount of seconds, unless it is reset or stopped before that.

### tick

```perl6
method tick() returns Duration
```

Emits the duration since creation time (or the last `reset`) to the Supply. Returns the emitted Duration.

### reset

```perl6
method reset(
    Numeric $in?
) returns Bool
```

Reset sets the start time for the stopwatch to the current time. If called with a numeric argument, it also sets a timer to expire after the given duration. It returns True if the call interrupted a pending timer, and False if no timer had been set, or if it had already expired, or if the stopwatch had already been stopped.

### stop

```perl6
method stop() returns Bool
```

Stop marks this stopwatch as done. Once a stopwatch has been stopped, most of its actions are finished, and new stopwatch should be created. Internally, it stops any pending timers and closes the supply. It returns True if the call interrupted a pending timer, and False if no timer had been set, or if it had already expired, or if the stopwatch had already been stopped.

### Duration

```perl6
method Duration() returns Duration
```

Returns the amount of time elapsed since the start (or the last reset) as a Duration.

### Promise

```perl6
method Promise() returns Promise
```

Returns a Promise that will be kept when the Stopwatch is stopped. When kept, it will hold the Duration elapsed since the time the Stopwatch was created, or since the last call to `reset`.

### Supply

```perl6
method Supply() returns Supply
```

Returns a live Supply that will receive timer events

## AUTHOR

José Joaquín Atria <jjatria@cpan.org>

## COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

